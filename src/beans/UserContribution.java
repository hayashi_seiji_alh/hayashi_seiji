package beans;

import java.io.Serializable;
import java.util.Date;

public class UserContribution implements Serializable {
		private static final long serialVersionUID = 1L;

		private int id;
		private String subject;
		private String body;
		private String category;
		private String name;
		private int userId;
		private Date createdDate;

		public int getId(){
			return id;
		}
		public void setId(int id){
			this.id = id;
		}
		public String getSubject(){
			return subject;
		}
		public void setSubject(String subject){
			this.subject = subject;
		}

		public String getBody(){
			return body;
		}
		public void setBody(String body){
			this.body = body;
		}

		public String getCategory(){
			return category;
		}
		public void setCategory(String category){
			this.category = category;
		}

		public String getName(){
			return name;
		}
		public void setName(String name){
			this.name = name;
		}

		public int getUserId(){
			return userId;
		}
		public void setUserId(int userId){
			this.userId = userId;
		}

		public Date getCreatedDate(){
			return createdDate;
		}
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}


}
