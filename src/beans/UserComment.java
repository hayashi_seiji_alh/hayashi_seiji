package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;;
	private String body;
	private String name;
	private int contributionId;
	private int userId;
	private Date createdDate;

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public String getBody(){
		return body;
	}
	public void setBody(String body){
		this.body = body;
	}

	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}

	public int getContributionId(){
		return contributionId;
	}
	public void setContributionId(int contributionId){
		this.contributionId = contributionId;
	}

	public int getUserId(){
		return userId;
	}
	public void setUserId(int userId){
		this.userId = userId;
	}

	public Date getCreatedDate(){
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
