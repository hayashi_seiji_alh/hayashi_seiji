package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String body;
	private int userId;
	private int contributionId;
	private Date createdDate;
	private Date updatedDate;

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public String getBody(){
		return body;
	}
	public void setBody(String body){
		this.body = body;
	}

	public int getUserId(){
		return userId;
	}
	public void setUserId(int userId){
		this.userId = userId;
	}
	public int getContributionId(){
		return contributionId;
	}
	public void setContributionId(int contributionId){
		this.contributionId = contributionId;
	}
	public Date getCreatedDate(){
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate(){
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
