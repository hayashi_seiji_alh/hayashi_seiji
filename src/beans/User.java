package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String password;
	private String name;
	private int branchId;
	private int positionId;
	private byte isStopped = 0;
	private Date createdDate;
	private Date 	updatedDate;

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public String getLoginId(){
		return loginId;
	}
	public void setLoginId(String loginId){
		this.loginId = loginId;
	}
	public String getPassword(){
		return password;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public int getBranchId(){
		return branchId;
	}
	public void setBranchId(int branchId){
		this.branchId = branchId;
	}
	public int getPositionId(){
		return positionId;
	}
	public void setPositionId(int positionId){
		this.positionId = positionId;
	}
	public byte getIsStopped(){
		return isStopped;
	}
	public void setIsStopped(byte isStopped){
		this.isStopped = isStopped;
	}
	public Date getCreateDate(){
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdateDate(){
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
