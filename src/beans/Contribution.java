package beans;

import java.io.Serializable;
import java.util.Date;

public class Contribution implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String subject;
	private String body;
	private String category;
	private int userId;
	private Date createdDate;
	private Date updatedDate;

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public String getSubject(){
		return subject;
	}
	public void setSubject(String subject){
		this.subject = subject;
	}

	public String getBody(){
		return body;
	}
	public void setBody(String body){
		this.body = body;
	}

	public String getCategory(){
		return category;
	}
	public void setCategory(String category){
		this.category = category;
	}

	public int getUserId(){
		return userId;
	}
	public void setUserId(int userId){
		this.userId = userId;
	}

	public Date getCreatedDate(){
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate(){
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
