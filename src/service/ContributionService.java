package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Contribution;
import beans.UserContribution;
import dao.ContributionDao;
import dao.UserContributionDao;


public class ContributionService {
	public void register(Contribution contribution){
        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contributionDao = new ContributionDao();
            contributionDao.insert(connection, contribution);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public List<UserContribution> getUserContribution(String start , String end,String category) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserContributionDao contributionDao = new UserContributionDao();
	        List<UserContribution> userContribution = contributionDao.getUserContributionList(connection,start,end,category);

	        commit(connection);

	        return userContribution;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public void deleteContribution(int contributionId){
		Connection connection = null;
		try{
			connection =getConnection();

			ContributionDao contributionDao = new ContributionDao();
			contributionDao.delete(connection,contributionId);

			commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
