package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.UserDao;

public class IsDuplicatedService {
	public boolean isDuplicated(String loginId){
	     Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            User user = userDao.selectLoginId(connection, loginId);
	            if(user.getLoginId() != null){
	            	return true;
	            }else{
	            	return false;
	            }


	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	}
	public boolean isDuplicatedEdit(int id,String loginId){
	     Connection connection = null;
	        try {
	            connection = getConnection();
		            UserDao userDao = new UserDao();

		            User user  = userDao.selectLoginIdNotInId(connection,id, loginId);
	            if(user.getLoginId() != null){
	            	return true;
	            }else{
	            	return false;
	            }

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
		            throw e;
	        } finally {
	            close(connection);
	        }
		}
}
