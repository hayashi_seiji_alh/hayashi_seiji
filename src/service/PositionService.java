package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Position;
import dao.PositionDao;

public class PositionService {
	public List<Position> getPosition(){
	    Connection connection = null ;
	    try {
	        connection = getConnection();

	        PositionDao positionDao = new PositionDao();
	        List<Position> positionList = positionDao.getPositionList(connection);

	        commit(connection);

	        return positionList;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}
