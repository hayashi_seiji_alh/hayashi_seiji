package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {
	public void register(User user){
		Connection connection = null;

		try{
			connection = getConnection();

			 String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);

	            UserDao userDao = new UserDao();
	            userDao.insert(connection, user);

	            commit(connection);
		} catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<User> getUser(){
	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        List<User> userList = userDao.getUserList(connection);

	        commit(connection);

	        return userList;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public User getEditUser(int userId){
		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			User editUser = userDao.selectEditUser(connection,userId);

			return editUser;
		} catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public void edit(User user){
		Connection connection = null;
		try{
			connection =getConnection();
		if(user.getPassword() != null){
		String encPassword = CipherUtil.encrypt(user.getPassword());
		user.setPassword(encPassword);
		}

		UserDao userDao = new UserDao();
		userDao.updateUser(connection,user);

		commit(connection);
		} catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public boolean idCheck(int id){
		Connection connection = null;
		try{
			connection =getConnection();

		UserDao userDao = new UserDao();
		User user =	userDao.selectEditUser(connection,id);

		if(user.getId() ==0){
			return true;
		}else{
			return false;
		}

		} catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}

