package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.UserCommentDao;

public class CommentService {
	public void register(Comment comment){
        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<UserComment> getUserComment() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserCommentDao commentDao = new UserCommentDao();
	        List<UserComment> userComment = commentDao.getUserCommentList(connection);

	        commit(connection);

	        return userComment;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public void deleteComment(int commentId){
		Connection connection = null;
		try{
			connection =getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection,commentId);

			commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}
