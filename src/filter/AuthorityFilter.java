package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/management","/edit","/signup"})
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
System.out.println("権限フィルター");
		User user = new User();
		HttpSession session = ((HttpServletRequest) request).getSession();
		user = (User) session.getAttribute("loginUser");


		if(user == null){
System.out.println("user==null");
			String message = "ログインしてください";
			session.setAttribute("errorMessages", message);
			((HttpServletResponse) response).sendRedirect("login");
			return;
		}

		if(user.getBranchId() == 1 && user.getPositionId() == 1){
System.out.println("user.getBranchId() == 1 && user.getPositionId() == 1");
			chain.doFilter(request,response);
			return;
		}else if(!(user.getBranchId() == 1 && user.getPositionId() == 1)){
System.out.println("!(user.getBranchId() == 1 && user.getPositionId() == 1)");
			String message = "アクセス権限がありません";
			session.setAttribute("errorMessages", message);
			((HttpServletResponse) response).sendRedirect("./");
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void destroy() {

	}
}
