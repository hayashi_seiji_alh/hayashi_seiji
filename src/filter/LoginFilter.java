package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns = {"/*"})
public class LoginFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request,ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		String path =((HttpServletRequest) request).getServletPath();
System.out.println("ログインフィルター");
		if(path.equals("/login")||path.equals("/css/login")||path.equals("/css/style.css")){
			chain.doFilter(request, response);
		return;
		}
		if(session.getAttribute("loginUser") != null){
			chain.doFilter(request, response);
			return;
		}else{
			String message = "ログインしてください";
			session.setAttribute("errorMessages", message);
			((HttpServletResponse) response).sendRedirect("login");
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {

	}
	@Override
	public void destroy() {

	}

}