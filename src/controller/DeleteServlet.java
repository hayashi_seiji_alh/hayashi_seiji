package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;
import service.ContributionService;

@WebServlet(urlPatterns = {"/delete"})
public class DeleteServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request ,
			HttpServletResponse response)throws IOException , ServletException{

		 if((request.getParameter("deleteContributionId")) != null){
				new ContributionService().deleteContribution(Integer.parseInt(request.getParameter("deleteContributionId")));

			}else if((request.getParameter("deleteCommentId")) != null){
				new CommentService().deleteComment(Integer.parseInt(request.getParameter("deleteCommentId")));

			}
			response.sendRedirect("./");
	}
}

