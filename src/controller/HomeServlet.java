package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserContribution;
import service.CommentService;
import service.ContributionService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request ,
			HttpServletResponse response)throws IOException , ServletException{

		request.setCharacterEncoding("UTF-8");

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category = request.getParameter("category");

		if(end == null || end == ""){
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			end =fmt.format(calendar.getTime());
			request.setAttribute("end", "");
		}else{
			request.setAttribute("end",end);
		}
		if(start == null || start == ""){
			start = "2018-01-01";
			request.setAttribute("start","");
		}else{
			request.setAttribute("start", start);
		}
		if(category == null){
			category = "";
		}


		List<UserContribution> contributions = new ContributionService().getUserContribution(start,end,category);
		List<UserComment> comments = new CommentService().getUserComment() ;

		request.setAttribute("contributions",contributions);
		request.setAttribute("comments", comments);
		request.setAttribute("category", category);

		request.getRequestDispatcher("home.jsp").forward(request,response);
	}

	protected void doPost(HttpServletRequest request ,
			HttpServletResponse response)throws IOException , ServletException{
		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category = request.getParameter("category");

		if(end == "" ){
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			end =fmt.format(calendar.getTime());
		}
		if(start == ""){
			start = "2018-01-01";
		}

		List<UserContribution> contributions = new ContributionService().getUserContribution(start,end,category);
		List<UserComment> comments = new CommentService().getUserComment() ;

		request.setAttribute("contributions",contributions);
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("home.jsp").forward(request, response);

	}
}