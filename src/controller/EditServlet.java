package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.IsDuplicatedService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response)throws IOException,ServletException{
		User editUser = new User();
		String messages;
		String id = request.getParameter("id");
		UserService userService = new UserService();

		HttpSession session = request.getSession();
		if(id==null){
			messages = "不正なIDが入力されました";
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
		if(!(id.matches("^[0-9]{0,}$"))){
			messages = "不正なIDが入力されました";
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
		if(id.isEmpty() ||userService.idCheck(Integer.parseInt(id))){
			messages = "不正なIDが入力されました";
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}

		editUser = new UserService().getEditUser(Integer.parseInt(request.getParameter("id")));


		List<Branch> branches = new BranchService().getBranch();
		List<Position>positions = new PositionService().getPosition();


		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response)throws IOException,ServletException{

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));

		if(!(request.getParameter("password").isEmpty())){
			editUser.setPassword(request.getParameter("password"));
		}
		List<String> messages = new ArrayList<String>();
		if(isValid(request,messages)){

			new UserService().edit(editUser);
			response.sendRedirect("management");
		}else{
			HttpSession session =request.getSession();
			session.setAttribute("errorMessages", messages);

			List<Branch> branches = new BranchService().getBranch();
			List<Position>positions = new PositionService().getPosition();

			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request , List<String> messages){
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");
		String loginId =  request.getParameter("loginId");
		String name = request.getParameter("name");
		int positionId = Integer.parseInt(request.getParameter("positionId"));
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int id = Integer.parseInt(request.getParameter("id"));



		if(loginId.isEmpty()){
			messages.add("ログインIDを入力してください");
		}else if(!(loginId.matches("^[0-9a-zA-z]{6,20}$"))){
			messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");
		}else if(new IsDuplicatedService().isDuplicatedEdit(id,loginId)){
			messages.add("すでに使用されているログインIDです");
		}

		if(password.isEmpty()){

		}else if(!(password.equals(confirmationPassword))){
			messages.add("パスワードが一致しません");
		}else if(!(password.matches("^[-_@+*;:#$%0-9a-zA-Z]{6,20}$"))){
			messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
		}

		if(name.isEmpty()){
			messages.add("名前を入力してください");
		}else if(!(name.length() <= 10)){
			messages.add("名前は10文字以下で入力してください");
		}

		if(branchId == 1){
			if(!(positionId == 1 || positionId == 11)){
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}else if(branchId != 1){
			if(positionId == 1 || positionId == 11){
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}




		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
