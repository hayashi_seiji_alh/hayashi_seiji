package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request ,
			HttpServletResponse response)throws IOException , ServletException{
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("loginUser");

		List<String> messages = new ArrayList<String>();

		if(isValid(request,messages) == true){
			System.out.println(request.getParameter("contributionId"));
			Comment comment = new Comment();
			comment.setBody(request.getParameter("body"));
			comment.setContributionId(Integer.parseInt(request.getParameter("contributionId")));
			comment.setUserId(user.getId());

			new CommentService().register(comment);

			response.sendRedirect("./");
		}else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request,List<String> messages){
		String comment = request.getParameter("body");

		if(StringUtils.isBlank(comment) == true){
			messages.add("メッセージを入力してください");
		}
		if(!(comment.length() <= 500)){
			messages.add("500文字以下で入力してください");
		}

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
