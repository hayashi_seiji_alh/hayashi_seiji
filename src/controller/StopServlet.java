package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.StopService;

@WebServlet(urlPatterns = {"/stop"})
public class StopServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request ,
			HttpServletResponse response)throws IOException , ServletException{
		int id = Integer.parseInt(request.getParameter("id"));
		Byte isStopped = (byte) Integer.parseInt(request.getParameter("isStopped"));

		StopService stopService = new StopService();
		stopService.userStop(id,isStopped);

		response.sendRedirect("management");
	}
}
