package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.IsDuplicatedService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request ,
			HttpServletResponse response)throws IOException , ServletException{
		List<Branch> branches = new BranchService().getBranch();
		List<Position>positions = new PositionService().getPosition();

		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("signup.jsp").forward(request,response);
	}
	@Override
	protected void doPost(HttpServletRequest request ,
			HttpServletResponse response)throws IOException,ServletException{
		List<String> messages = new ArrayList<String>();

		User user = new User();
		user.setLoginId(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

		if(isValid(request,messages)){
			new UserService().register(user);
			response.sendRedirect("management");
		}else{
			HttpSession session =request.getSession();
			session.setAttribute("errorMessages", messages);

			List<Branch> branches = new BranchService().getBranch();
			List<Position>positions = new PositionService().getPosition();

			request.setAttribute("signupUser", user);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("signup.jsp").forward(request, response);;

		}
	}
	private boolean isValid(HttpServletRequest request , List<String> messages){
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");
		String loginId =  request.getParameter("loginId");
		String name = request.getParameter("name");
		int positionId = Integer.parseInt(request.getParameter("positionId"));
		int branchId = Integer.parseInt(request.getParameter("branchId"));


		if(loginId.isEmpty()){
			messages.add("ログインIDを入力してください");
		}else if(!(loginId.matches("^[0-9a-zA-z]{6,20}$"))){
			messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");
		}else if(new IsDuplicatedService().isDuplicated(loginId)){
			messages.add("すでに使用されているログインIDです");
		}

		if(password.isEmpty()){
			messages.add("パスワードを入力してください");
		}else if(!(password.equals(confirmationPassword))){
			messages.add("パスワードが一致しません");
		}else if(!(password.matches("^[-_@+*;:#$%0-9a-zA-Z]{6,20}$"))){
			messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
		}

		if(confirmationPassword.isEmpty()){
			messages.add("確認用パスワードを入力してください");
		}

		if(name.isEmpty()){
			messages.add("名前を入力してください");
		}else if(!(name.length() <= 10)){
			messages.add("名前は10文字以下で入力してください");
		}

		if(branchId == 1){
			if(!(positionId == 1 || positionId == 11)){
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}else if(branchId != 1){
			if(positionId == 1 || positionId == 11){
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
