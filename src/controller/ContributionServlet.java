package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import beans.User;
import service.ContributionService;

@WebServlet(urlPatterns = {"/contribution"})
public class ContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response)throws IOException,ServletException{
		request.getRequestDispatcher("contribution.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response)throws IOException,ServletException{
		HttpSession session = request.getSession();

		User user = (User)session.getAttribute("loginUser");

		Contribution contribution = new Contribution();
		contribution.setSubject(request.getParameter("subject"));
		contribution.setBody(request.getParameter("body"));
		contribution.setCategory(request.getParameter("category"));
		contribution.setUserId(user.getId());

		List<String> messages = new ArrayList<String>();
		if(isValid(request,messages) == true){

		new ContributionService().register(contribution);

		response.sendRedirect("./");

		}else{
			session.setAttribute("errorMessages", messages);
			System.out.println(request.getParameter("subject"));
			request.setAttribute("subject",request.getParameter("subject"));
			request.setAttribute("body",request.getParameter("body"));
			request.setAttribute("category",request.getParameter("category"));
			request.getRequestDispatcher("contribution.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request,List<String> messages){
		String subject = request.getParameter("subject");
		String body = request.getParameter("body");
		String category = request.getParameter("category");
		if(StringUtils.isBlank(subject)==true){
			messages.add("件名を入力してください");
		}
		if(!(subject.length() <= 30)){
			messages.add("件名は30文字以下で入力してください");
		}

		if(StringUtils.isBlank(body)==true){
			messages.add("本文を入力してください");
		}
		if(!(body.length() <= 1000)){
			messages.add("本文は1000文字以下で入力してください");
		}

		if(StringUtils.isBlank(category)==true){
			messages.add("カテゴリーを入力してください");
		}
		if(!(category.length() <= 10)){
			messages.add("カテゴリーは10文字以下で入力してください");
		}

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
