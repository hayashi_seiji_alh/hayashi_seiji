package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {
	public void insert(Connection connection , User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users(");
			sql.append("login_id");
			sql.append(",password");
			sql.append(",name");
			sql.append(",branch_id");
			sql.append(",position_id");
			sql.append(",is_stopped");
			sql.append(",created_date");
			sql.append(",updated_date");
			sql.append(")VALUES(");
			sql.append("?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",0");
			sql.append(",CURRENT_TIMESTAMP");
			sql.append(",CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setString(4, String.valueOf(user.getBranchId()));
			ps.setString(5, String.valueOf(user.getPositionId()));
			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
				close(ps);

		}
	}

	public User getLoginUser(Connection connection,String loginId,String password ){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1,loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty()==true){
				return null;
			}else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			}else{
				return userList.get(0);
			}
		}catch(SQLException e){
			 throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> userList = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String loginId = rs.getString("login_id");
	            String password = rs.getString("password");
	            String name = rs.getString("name");
	            int branchId = rs.getInt("branch_id");
	            int positionId = rs.getInt("position_id");
	            byte isStopped = rs.getByte("is_stopped");
	            Timestamp createdDate = rs.getTimestamp("created_date");
	            Timestamp updatedDate = rs.getTimestamp("updated_date");

	            User user = new User();
	            user.setId(id);
	            user.setLoginId(loginId);
	            user.setPassword(password);
	            user.setName(name);
	            user.setBranchId(branchId);
	            user.setPositionId(positionId);
	            user.setIsStopped(isStopped);
	            user.setCreatedDate(createdDate);
	            user.setUpdatedDate(updatedDate);

	            userList.add(user);
	        }
	        return userList;
	    }finally {
	        close(rs);
	    }
	}

    public List<User> getUserList(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM users");
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> userList  = toUserList(rs);
            return userList ;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User selectEditUser(Connection connection,int userId){
    	PreparedStatement ps = null;
    	try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();
			User user = new User();
			while(rs.next()){
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setPositionId(rs.getInt("position_Id"));
				user.setIsStopped(rs.getByte("is_stopped"));
				user.setCreatedDate(rs.getDate("created_date"));
				user.setUpdatedDate(rs.getDate("updated_date"));
			}
			return user;
    	}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
				close(ps);

		}
    }
    public void updateUser(Connection connection ,User user){
    	PreparedStatement ps = null;
    	try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			System.out.println(user.getPassword());
			if(user.getPassword() != null){
				sql.append(", password = ?");
			}
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());


			ps.setString(1, user.getLoginId());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getBranchId());
            ps.setInt(4, user.getPositionId());
            if(user.getPassword() !=null){
            	ps.setString(5,user.getPassword());
            	ps.setInt(6, user.getId());
            }else{
            	ps.setInt(5, user.getId());
            }

            ps.executeUpdate();


	    }catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
				close(ps);
		}
    }
    public void stop(Connection connection , int id , Byte isStopped){
    	PreparedStatement ps = null;
    	try{
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE users SET");
    		sql.append(" is_stopped = ?");
    		sql.append(" WHERE");
    		sql.append(" id = ?");

    		ps = connection.prepareStatement(sql.toString());

    		if(isStopped == 0){
    			ps.setByte(1,(byte) 1);
    		}else if(isStopped == 1){
    			ps.setByte(1, (byte) 0);
    		}

    		ps.setInt(2, id);

    		ps.executeUpdate();

    	}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
				close(ps);
		}
    }
    public User selectLoginId(Connection connection ,String loginId){
    	PreparedStatement ps = null;
    	try{
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT * FROM users");
    		sql.append(" WHERE login_id = ?");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setString(1, loginId);

    		ResultSet rs = ps.executeQuery();

			User user = new User();
			while(rs.next()){
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setPositionId(rs.getInt("position_Id"));
				user.setIsStopped(rs.getByte("is_stopped"));
				user.setCreatedDate(rs.getDate("created_date"));
				user.setUpdatedDate(rs.getDate("updated_date"));
			}
			return user;

    	}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
				close(ps);
		}
    }
    public User selectLoginIdNotInId(Connection connection,int id ,String loginId){
    	PreparedStatement ps = null;
    	try{
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT * FROM users");
    		sql.append(" WHERE login_id = ?");
    		sql.append(" AND id NOT IN(?)");
    		ps = connection.prepareStatement(sql.toString());

    		ps.setString(1, loginId);
    		ps.setInt(2, id);
System.out.println(ps);
    		ResultSet rs = ps.executeQuery();

			User user = new User();
			while(rs.next()){
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setPositionId(rs.getInt("position_Id"));
				user.setIsStopped(rs.getByte("is_stopped"));
				user.setCreatedDate(rs.getDate("created_date"));
				user.setUpdatedDate(rs.getDate("updated_date"));
			}
			return user;

    	}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
				close(ps);
		}
    }
}
