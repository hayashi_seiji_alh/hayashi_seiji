package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {
	public List<UserComment> getUserCommentList(Connection connection){

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.body as body, ");
            sql.append("comments.contribution_id as contribution_id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

        List<UserComment> commentList  = toUserCommentList(rs);
		return commentList;
        }catch(SQLException e){
            throw new SQLRuntimeException(e);
        }finally{
        	close(ps);
        }
	}
    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> commentList = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String body = rs.getString("body");
                String name	= rs.getString("name");
                int contributionId = rs.getInt("contribution_id");
                int userId = rs.getInt("user_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment comment = new UserComment();
                comment.setId(id);
                comment.setBody(body);
                comment.setName(name);
                comment.setContributionId(contributionId);
                comment.setUserId(userId);
                comment.setCreatedDate(createdDate);

                commentList.add(comment);
            }
            return commentList;
        } finally {
            close(rs);
        }
    }
}
