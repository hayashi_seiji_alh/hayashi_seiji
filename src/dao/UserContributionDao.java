package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserContribution;
import exception.SQLRuntimeException;


public class UserContributionDao {
    public List<UserContribution> getUserContributionList(Connection connection,String start,String end,String category) {
System.out.println(start +","+ end);
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("contributions.id as id, ");
            sql.append("contributions.subject as subject, ");
            sql.append("contributions.body as body, ");
            sql.append("contributions.category as category, ");
            sql.append("contributions.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("contributions.created_date as created_date ");
            sql.append("FROM contributions ");
            sql.append("INNER JOIN users ");
            sql.append("ON contributions.user_id = users.id ");
            sql.append("WHERE contributions.created_date ");
            sql.append("BETWEEN ? ");
            sql.append("AND ? ");
            sql.append("AND contributions.category LIKE ? ");
            sql.append("ORDER BY created_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1 , start +" 00:00:00");
            ps.setString(2 , end + " 23:59:59");
            ps.setString(3, "%"+category+"%");

            ResultSet rs = ps.executeQuery();
            List<UserContribution> contributionList  = toUserContributionList(rs);
            return contributionList ;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserContribution> toUserContributionList(ResultSet rs)
            throws SQLException {

        List<UserContribution> contributionList = new ArrayList<UserContribution>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String subject = rs.getString("subject");
                String body = rs.getString("body");
                String category = rs.getString("category");
                String name = rs.getString("name");
                int userId = rs.getInt("user_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserContribution contribution = new UserContribution();
                contribution.setId(id);
                contribution.setSubject(subject);
                contribution.setBody(body);
                contribution.setCategory(category);
                contribution.setName(name);
                contribution.setCreatedDate(createdDate);
                contribution.setUserId(userId);

                contributionList.add(contribution);
            }
            return contributionList;
        } finally {
            close(rs);
        }
    }
}
