package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {
	   public List<Position> getPositionList(Connection connection) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT * FROM positions");
	            ps = connection.prepareStatement(sql.toString());

	            ResultSet rs = ps.executeQuery();
	            List<Position> positionList  = toPositionList(rs);
	            return positionList ;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }
		private List<Position> toPositionList(ResultSet rs) throws SQLException {

		    List<Position> positionList = new ArrayList<Position>();
		    try {
		        while (rs.next()) {
		            int id = rs.getInt("id");
		            String name = rs.getString("name");
		            Timestamp createdDate = rs.getTimestamp("created_date");
		            Timestamp updatedDate = rs.getTimestamp("updated_date");

		            Position position = new Position();
		            position.setId(id);
		            position.setName(name);
		            position.setCreatedDate(createdDate);
		            position.setUpdatedDate(updatedDate);

		            positionList.add(position);
		        }
		        return positionList;
		    }finally {
		        close(rs);
		    }
		}
}
