<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿画面</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<link href="./css/style_contribution.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<a href="./">ホームへ戻る</a>
		<c:if test = "${not empty errorMessages}">
			<div class = "errorMessages">
				<ul>
					<c:forEach items = "${errorMessages}" var = "errorMessage">
						<li><c:out value = "${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<div class = "main-contents">
			<form action = "contribution" method = "post">
				<input name = "subject" value = "${subject}" class ="text" size = "50" placeholder="タイトル（30文字以内で入力してください）" /><br/>
				<input name ="category" value = "${category}" class ="text" size = "50" placeholder="カテゴリー（10文字以内で入力してください）" ><br/>
				<textarea class = "text" name = "body" cols = "55"  rows = "10"  placeholder="本文（1000文字以内で入力してください）">${body}</textarea><br/>

				<input class ="submit-button" type = "submit" value = "投稿する"><br/>
			</form>
		</div>
		<div class="copyright"> Copyright(c)hayashi_seiji</div>

	</body>
</html>