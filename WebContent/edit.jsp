<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored = "false" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<link href="./css/style_edit.css" rel="stylesheet" type="text/css">
	<title>ユーザー編集画面</title>
	</head>
	<body>
		<a href="management">ユーザー管理画面へ戻る</a>
		<c:if test = "${not empty errorMessages}">
			<div class = "errorMessages" >
			<ul>
				<c:forEach items = "${errorMessages}" var = "errorMessage">
					<li><c:out value = "${errorMessage}" />
				</c:forEach>
			</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
		<div class = "main-contents">
		<form action = "edit" method = "post">
			<input name = "id" value = "${editUser.id}" id = "id" type = "hidden" class ="text">
			<label for = "loginId">ログインID</label><br/>
			<input name ="loginId" value = "${editUser.loginId}" id = "loginId" class ="text"><br/>
			<label for = "password">パスワード</label><br/>
			<input type = "password" name ="password" id = "password" class ="text"><br/>
			<label for = "confirmationPassword">パスワードの確認</label><br/>
			<input type = "password" name ="confirmationPassword"  id = "confirmationPassword" class ="text"><br/>
			<label for = "name">ユーザー名</label><br/>
			<input name ="name" value = "${editUser.name}" id = "name" class ="text"><br/>

			<c:if test = "${editUser.id!=loginUser.id}">
				<label for = "branchId">支店
				</label><br/>
				<select name = "branchId" id = "branchId">

					<c:forEach items = "${branches}" var = "branch">
						<c:if test = "${editUser.branchId != branch.id}">
							<option value = "${branch.id}" label = "${branch.name}"></option>
						</c:if>
						<c:if test = "${editUser.branchId == branch.id}">
							<option value = "${branch.id}" label = "${branch.name}" selected ="${branch.name}">
						</c:if>
					</c:forEach>
				</select>
				<br/>

				<label for = "positionId">部署・役職
				</label><br/>
				<select name = "positionId" id = "positionId">
					<c:forEach items = "${positions}" var = "position">
						<c:if test = "${editUser.positionId != position.id}">
							<option value = "${position.id}" label = "${position.name}"></option>
						</c:if>
						<c:if test = "${editUser.positionId == position.id}">
							<option value = "${position.id}" label = "${position.name}" selected ="${position.name}">
						</c:if>
					</c:forEach>
				</select>
				<br/>
			</c:if>

			<c:if test = "${editUser.id==loginUser.id}">
			<label for = "branchId">支店</label><br/>
			<select name = "branchId" id = "branchId">

					<c:forEach items = "${branches}" var = "branch">
						<c:if test = "${editUser.branchId == branch.id}">
							<option value = "${branch.id}" label = "${branch.name}" selected ="${branch.name}">
						</c:if>
					</c:forEach>
				</select>
				<br/>

				<label for = "positionId">部署・役職</label><br/>
				<select name = "positionId" id = "positionId">
					<c:forEach items = "${positions}" var = "position">
						<c:if test = "${editUser.positionId == position.id}">
							<option value = "${position.id}" label = "${position.name}" selected ="${position.name}">
						</c:if>
					</c:forEach>
				</select>
				<br/>
			</c:if>

			<div class="submit-button"><input class = "submit-button" type = "submit" value = "修正する"></div>
		</form>
		</div>
		<div class="copyright"> Copyright(c)hayashi_seiji</div>
	</body>
</html>