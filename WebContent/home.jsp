<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored = "false" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ホーム画面</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<link href="./css/style_home.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<h1>ホーム画面</h1>
		<c:if test = "${not empty errorMessages}">
				<div class = "errorMessages">
			<ul>
				<c:forEach items = "${errorMessages}" var = "errorMessage">
					<li><c:out value = "${errorMessage}" />
				</c:forEach>
			</ul>
			</div>
			  <c:remove var="errorMessages" scope="session"/>
		</c:if>

		<div class = "menu">
			<div class = "home-title">・メニュー</div><br/>
			<br/>
			<a href = "logout">ログアウト</a><br/>
			<c:if test = "${loginUser.branchId == 1 && loginUser.positionId == 1 }">
				<a href ="management" >ユーザー管理画面</a><br/>
			</c:if>
			<a href ="contribution" >投稿を作成する</a><br/>
			<br/>
		<div class = "refine">
				<div class = "home-title">・検索</div><br/>
				<br/>
				<form action = "./" method = "get">
				<div class = "refine-form"><label for = "start">開始日</label><input type = "date"  name = "start" value="${start}" id ="start" ></div>
				<div class = "refine-form"><label for = "end">終了日</label><input type = "date"  name = "end" value="${end}" id ="end" ></div>
				<div class = "refine-form"><label for = "category">カテゴリー</label><input value = "${category}" name = "category" id = "category"></div>
				<div class ="refine-button"><input class = "submit-button" type = "submit" value = "絞り込む"></div>
				</form>
			</div>
		</div>
		<div class = "contributions">

			<c:forEach items ="${contributions}" var ="contribution" >
<div class = "contribution-block" >
				<div class = "contribution-subject">件名：<c:out value = "${contribution.subject}" /></div>
				<div class = "contribution-category">カテゴリー：<c:out value = "${contribution.category}" /></div>
				<div class = "date"><fmt:formatDate value = "${contribution.createdDate}" pattern = "yyyy/MM/dd HH:mm:ss" /></div>
				<div class = "name">投稿者：<c:out value = "${contribution.name}" /></div>

				<br/>
				<c:forEach var="contributionBody" items="${ fn:split(contribution.body,'
				') }"><div class = "text"><c:out value = "${contributionBody}" /></div>
				</c:forEach>


				<c:if test = "${contribution.userId == loginUser.id}">
					<form action = "delete" method = "post">
						<input type = "hidden" name = "deleteContributionId" value ="${contribution.id}" />
						<div class= "submit-button"><input class="delete-submit-button" type = "submit" value = "削除" /></div>
					</form>
				</c:if>
</div>
<div class = "commentToContribution-form">
				<form action = "comment" method = "post">
					<div class ="comment-textarea"><textarea name = "body" cols = "110" rows = "5" placeholder ="${contribution.subject}へコメントする" ></textarea></div>
					<input type = "hidden" name = "contributionId" value = "${contribution.id}" />
					<input id = "comment-submit" type = "submit" value = "送信"><br/>
				</form>
</div>
<br/>
				<div class="comment">
				<c:if test = "${not empty comments}">
					<div class = "comment-title" >${contribution.subject}へのコメント</div>
				</c:if>
				<c:forEach items = "${comments}" var = "comment">
				<div class="comment-content">
					<c:if test = "${contribution.id  == comment.contributionId}">
					<div class = "comment-block">
						<div class = "name">投稿者：<c:out value = "${comment.name}" /></div>
						<div class = "date"><c:out value = "${comment.createdDate}" /></div>
						<c:forEach var="commentBody" items="${ fn:split(comment.body,'
						') }">
						<div class = "text" id = "comment-body"><c:out value = "${commentBody}" /></div>
						</c:forEach>


						<c:if test = "${comment.userId == loginUser.id }">
							<form action = "delete" method = "post">
								<input type = "hidden" name = "deleteCommentId" value = "${comment.id}" />
								<div class ="submit-button"><input class="delete-submit-button" type = "submit" value = "削除" /></div>
							</form>
						</c:if>
					</div>
					</c:if>
				</div>
				</c:forEach>
				</div>
			</c:forEach>
		</div>
		<div class="copyright"> Copyright(c)hayashi_seiji</div>
	</body>
</html>