<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored = "false" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録画面</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<link href="./css/style_signup.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<a href="management">ユーザー管理画面へ戻る</a>
		<c:if test = "${not empty errorMessages}">
			<div class = "errorMessages">
			<ul>
				<c:forEach items = "${errorMessages}" var = "errorMessage">
					<li><c:out value = "${errorMessage}" />
				</c:forEach>
			</ul>
			</div>
		<c:remove var="errorMessages" scope="session"/>
		</c:if>
		<div class = "main-contents">
			<form action = "signup" method = "post"><br/>
				<label for = "loginId"></label><input name = "loginId" value = "${signupUser.loginId}" id = "loginId" placeholder="ログインID（半角英数字 6～20文字）"/><br/>
				<label for = "name"></label><input name = "name" value = "${signupUser.name}" id = "name" placeholder="ユーザー名（10文字以内）" /><br/>
				<label for = "password"></label>
				<input type = "password" name = "password"  id = "password" placeholder="パスワード（半角文字 6～20文字）"/><br/>
				<label for = "confirmationPassword"></label>
				<input type = "password" name = "confirmationPassword" id ="confirmationPassword" placeholder="パスワードの確認"/><br/>

				<div class = "select-form">
					<label for = "branchId">支店</label><br/>
					<span class ="select-box">
					<select name = "branchId" id = "branchId">
						<c:forEach items = "${branches}" var = "branch">
							<c:if test = "${signupUser.branchId != branch.id}">
								<option value = "${branch.id}" label = "${branch.name}"></option>
							</c:if>
							<c:if test = "${signupUser.branchId == branch.id}">
								<option value = "${branch.id}" label = "${branch.name}" selected ="${branch.name}">
							</c:if>
						</c:forEach>
					</select>
					</span>
					<br/>

					<label for = "positionId">部署・役職</label><br/>
					<span class ="select-box">
					<select name = "positionId" id = "positionId">
						<c:forEach items = "${positions}" var = "position">
							<c:if test = "${signupUser.positionId != position.id}">
								<option value = "${position.id}" label = "${position.name}"></option>
							</c:if>
							<c:if test = "${signupUser.positionId == position.id}">
								<option value = "${position.id}" label = "${position.name}" selected ="${position.name}">
							</c:if>
						</c:forEach>
					</select>
					</span>
					<br/>
				</div>
				<div class="submit-button"><input class = "submit-button" type = "submit" value = "登録する" /></div>
			</form>
		</div>
		<div class="copyright"> Copyright(c)hayashi_seiji</div>
	</body>
</html>