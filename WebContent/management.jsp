<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored = "false" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理画面</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<link href="./css/style_management.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">
	<!--
	function check(message){
		if(window.confirm(message + 'してよろしいですか？')){ // 確認ダイアログを表示
			return true; // 「OK」時は送信を実行
		}
		else{ // 「キャンセル」時の処理
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false; // 送信を中止
		}
	}

	// -->
	</script>
	</head>
	<body>
		<c:if test = "${not empty errorMessages }">
			<div class = "errorMessages">
				<ul>
					<c:forEach items = "${errorMessages}" var = "errorMessage" >
						<li><c:out value = "${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			  <c:remove var="errorMessages" scope="session"/>
		</c:if>

		<a href="./">ホームへ戻る</a><br/>
		<a href ="signup" >新しいユーザーを登録する</a>

		<div class = "main-contents">
		<h1>ユーザー一覧</h1>
		<table border = "1">
			<tr>
				<th class = "id">ユーザーID</th>
				<th class = "name">ユーザー名</th>
				<th class = "branch">支店</th>
				<th class = "position">部署・役職</th>
				<th class = "is-stopped">停止・復活</th>
				<th class = "edit">編集</th>
			</tr>
			<c:forEach items ="${users}" var ="user" >
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>

					<td>
					<c:forEach items = "${branches}" var = "branch">
						<c:if test="${user.branchId == branch.id }">
						${branch.name}
						</c:if>
					</c:forEach>
					</td>
					<td>
					<c:forEach items = "${positions}" var = "position">
						<c:if test="${user.positionId == position.id }">
						${position.name}
						</c:if>
					</c:forEach>
					</td>
					<td>
					<c:if test = "${user.id != loginUser.id}">
						<c:if test ="${user.isStopped == 0}" >
							<form action = "stop" name = "stop" method = "post" onSubmit ="return check('停止')" >
							<input type = "hidden" name = "id" value = "${user.id}">
							<input type = "hidden" name = "isStopped" value = "${user.isStopped}">
							<input id = "stop-button" type = "submit"  value = "停止">
							</form></c:if>
						<c:if test ="${user.isStopped == 1}" >
							<form action = "stop" name = "resurrection" method = "post" onSubmit = "return check('復活')" >
							<input type = "hidden" name = "id" value = "${user.id}">
							<input type = "hidden" name = "isStopped" value = "${user.isStopped}">
							<input id = "restoration-button" type = "submit"  value = "復活">
							</form>
						</c:if>
					</c:if>
					</td>

					<td><form action = "edit" method = "get">
					<input type = "hidden" name = "id" value = "${user.id}" id = "id" />
					<!-- <input id = "edit-button" type = "submit" value = "編集">  -->
					<a href = "edit?id=${user.id}" id = "edit-page">編集</a>

					</form></td>
				</tr>
				</c:forEach>
		</table>
		</div>
		<div class="copyright"> Copyright(c)hayashi_seiji</div>
	</body>
</html>