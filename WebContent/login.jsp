<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
			<c:if test = "${not empty errorMessages }">
			<div class = "errorMessages">
				<ul>
					<c:forEach items = "${errorMessages}" var = "errorMessage" >
						<li><c:out value = "${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
	<div class = "main-contents">

		<div class ="title"><h1>掲示板</h1></div>
		<div class = "login-form">
			<form action="login" method="post">
				<input name="loginId" id="loginId" value = "${loginId}"class= "form" placeholder="ログインID" /><br />
				<input name="password" type="password" id="password" class= "form" placeholder="パスワード"/><br />
				<div class = "submit-button"><input class = "submit-button" type="submit" value="ログイン" /> <br /></div>
			</form>
		</div>
	</div>
	<div class="copyright"> Copyright(c)hayashi_seiji</div>
	</body>
</html>